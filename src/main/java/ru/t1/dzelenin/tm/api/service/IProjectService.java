package ru.t1.dzelenin.tm.api.service;

import ru.t1.dzelenin.tm.api.repository.IProjectRepository;
import ru.t1.dzelenin.tm.enumerated.Sort;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

    void remove(Project project);

    List<Project> findAll();

    List<Project> findAll(Sort sort);

    List<Project> findAll(Comparator comparator);

    Project changeProjectStatusIndex(Integer index, Status status);

    Project changeProjectStatusId(String id, Status status);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project removeById(String id);

    Project removeByIndex(Integer index);
}
