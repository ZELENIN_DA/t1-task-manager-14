package ru.t1.dzelenin.tm.api.repository;

import ru.t1.dzelenin.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
